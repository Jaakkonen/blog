<!--
.. title: Testing boto3 unhappy cases
.. slug: testing-boto3-unhappy-cases
.. date: 2021-03-22 14:15:45 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Testing software is hard. Most tests check that happy cases do what they should. 
Essentially testing that X works when everything around it is ok.
That achieves a good level of reliability but knowing behavior when things inevidably go wrong is valuable. 
It helps avoiding bugs and figuring out what may be wrong when a actual disaster/non-happy case happens.
The bigger approach of designing for unexpected conditions and problems on system-level is referred to as [chaos engineering](https://en.wikipedia.org/wiki/Chaos_engineering). See [this](https://cucumber.io/blog/test-automation/happy-unhappy-paths-why-you-need-to-test-both/) blogpost from cucumber.io to see reasons why unhappy paths/cases should be tested.

Testing for happy cases is pretty easy with [moto](https://github.com/spulec/moto) which mocks AWS services enabling easy unittesting of AWS service calls in an application. However that library does not include a way for the mocked service to return a unhappy result. This can be done using [botocore](https://github.com/boto/botocore)'s `Stubber` as shown in [boto3#2485](https://github.com/boto/boto3/issues/2485#issuecomment-691347814) comment.

For instance when testing how a piece of code would behave, one can do something as follows using [pytest](TODO), [moto](TODO) and [boto3](TODO).

(Prepare demo env with)
```sh
cd $(mktemp -d)
python -m venv venv
. ./venv/bin/activate
pip install 'moto[sns]' pytest boto3
```

Write the demo code:

```python
import pytest
import boto3
from botocore.stub import Stubber
from botocore.exceptions import ClientError
from moto import mock_sns


@mock_sns
def test_sns_ok():
    sns = boto3.client("sns")
    topic_arn = sns.create_topic(Name="foo")["TopicArn"]
    res = sns.publish(TopicArn=topic_arn, Message="Hello world!")
    assert res["ResponseMetadata"]["HTTPStatusCode"] == 200


@mock_sns
def test_sns_internal_error():
    sns = boto3.client("sns")
    topic_arn = sns.create_topic(Name="foo")["TopicArn"]

    stubber = Stubber(sns)
    # List of errors/responses: https://docs.aws.amazon.com/sns/latest/api/API_Publish.html
    # Stubber docs: https://botocore.amazonaws.com/v1/documentation/api/latest/reference/stubber.html
    stubber.add_client_error(
        "publish",
        service_error_code="InternalError",
        service_message="Unknown exception",
        http_status_code=500,
    )
    stubber.activate()
    with pytest.raises(sns.exceptions.InternalErrorException) as excinfo:
        res = sns.publish(TopicArn=topic_arn, Message="Hello world!")
    exception = excinfo.value
    assert exception.response["ResponseMetadata"]["HTTPStatusCode"] == 500
    assert exception.args == (
        "An error occurred (InternalError) when calling the Publish operation: Unknown exception",
    )


@mock_sns
def test_sns_resource_api_error():
    # Resource APIs found by searching for "Service Resource"
    # in https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/index.html
    # For SNS: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sns.html#service-resource
    sns = boto3.resource("sns")
    topic = sns.create_topic(Name="foo")

    # Stub the client under resource: https://github.com/boto/botocore/issues/1125
    stubber = Stubber(sns.meta.client)
    stubber.add_client_error(
        "publish",
        service_error_code="InternalError",
        service_message="Unknown exception",
        http_status_code=500,
    )
    stubber.activate()

    with pytest.raises(sns.meta.client.exceptions.InternalErrorException) as excinfo:
        # Topic resouce doesn't document how exceptions work but they're the same as for using raw boto3.client('sns').
        # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sns.html#SNS.Topic.publish
        topic.publish(Message="Hello world!")

    exception = excinfo.value
    assert isinstance(exception, ClientError)
    assert exception.response["ResponseMetadata"]["HTTPStatusCode"] == 500
    assert exception.args == (
        "An error occurred (InternalError) when calling the Publish operation: Unknown exception",
    )
```

And these test functions with pytest

```sh
pytest <demofile>.py
```

Hopefully you learned something, have a wonderful day :)

