# List of possible blog topics

- "So Wayland is more secure than X11?"
    - Demo global keylogger and mouse capturer via layer-shell and virtual keyboard
    - Link security discussions and statuses from https://github.com/swaywm/sway/issues/984 and rabbit hole that leads to
    - Protecting via interceptin discussion with WAYLAND\_SOCKET?
- Integrating pylint/flakehell output to SonarQube/Gitlab/Bitbucket
- N00b networking: Sending structs over TLS with C, Rust and Python

